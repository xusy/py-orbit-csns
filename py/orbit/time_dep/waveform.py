import sys
import os
import math
from random import random, seed, gauss

from orbit.utils import NamedObject, TypedObject,orbitFinalize

class Waveform(NamedObject, TypedObject):
	"""
	The base abstract class of waveforms hierarchy.
	"""
	def __init__(self, name = "no name"):
		NamedObject.__init__(self, name)
		TypedObject.__init__(self, "base waveform")
		
class KickerWaveform(Waveform):
	"""
	The subclass of Waveform class. The abstract class of kicker waveforms.
	"""
	def __init__(self, name = "no name"):
		Waveform.__init__(self, name)
		self.setType("kicker waveform")
		self.kx = None
		self.ky = None
		
	def setKx(self,kx):
		self.kx = kx
	
	def getKx(self):
		return self.kx
		
	def setKy(self,ky):
		self.ky = ky
		
	def getKy(self):
		return self.ky

class ConstantKickerWaveform(KickerWaveform):
	"""
	The kicker waveform of constant strength.
	"""
	def __init__(self, name = "no name"):
		KickerWaveform.__init__(self, name)
		
	def initialize(self,kx,ky):
		self.setKx(kx)
		self.setKy(ky)
		
	def calc(self, time):		
		pass
		
class SquareRootKickerWaveform(KickerWaveform):
	"""
	Square Root Waveform of Kicker.
	"""
	def __init__(self, name = "no name"):
		KickerWaveform.__init__(self, name)
		self.__initial = []
		
	def initialize(self,t1,t2,kx,ky,dx,dy):				
		self.__initial = (t1,t2,dx,dy)
		self.setKx(kx)
		self.setKy(ky)
		
	def calc(self, time):
		t = time
		(t1,t2,dx,dy) = self.__initial
		dt = sqrt((t-t1)/(t2-t1))
		self.setKx(self.getKx()*(1-dt)+dx)
		self.setKy(self.getKy()*(1-dt)+dy)		
		
class MagnetWaveform(Waveform):
	"""
	The subclass of Waveform class. The abstract class of the magnet waveforms.
	"""
	def __init__(self, name = "no name"):
		Waveform.__init__(self, name)
		self.setType("magnet waveform")
		self.strength = None
	
	def setStrength(self,strength):
		self.strength = strength

	def getStrength(self):
		return self.strength

class ConstantMagnetWaveform(MagnetWaveform):
	"""
	The magnet waveform of constant strength.
	"""
	def __init__(self, strength, name = "no name"):
		MagnetWaveform.__init__(self, name)
		self.setStrength(strength)
	
	def calc(self,time):
		pass

class LinearMagnetWaveform(MagnetWaveform):
	"""
	Linear lattice strength variation between t1 and t2 
	"""
	def __init__(self, name = "no name"):
		MagnetWaveform.__init__(self, name)
		self.__initial = []
		
	def initialize(self,t1,t2,s1,s2):
		self.__initial = (t1,t2,s1,s2)
		
	def calc(self, t):		
		(t1,t2,s1,s2) = self.__initial
		if t<t1: self.strength = s1
		elif t>t2: self.strength = s2
		else:
		 # dt = math.sqrt((t-t1)/(t2-t1))
                  dt = (t-t1)/(t2-t1)#liyong modify
		  self.setStrength(s1+dt*(s2-s1))

class CustomMagnetWaveform(MagnetWaveform):
	"""
	This class get the factor from customize curve
        @Liyong/CSNS, liyong@ihep.ac.cn
	"""
	def __init__(self, timetuple, amptuple,name = "no name"):
		MagnetWaveform.__init__(self, name)
		self.__initial = ( timetuple, amptuple)

        def calc(self, t):
		(timetuple, amptuple) = self.__initial	
                n_tuple=len(timetuple)-1               	
		if t <= timetuple[0]: self.strength = amptuple[0]	    			    	
		if t >= timetuple[n_tuple]: self.strength = amptuple[n_tuple]	
		else:    	
	    		dtp = t - timetuple[0]
	    		for n in range(n_tuple):
	    			dtm = dtp
	    			dtp = t - timetuple[n + 1]
	    			dtmp = dtm * dtp
	    			if dtmp <= 0:
	    				break
	    		factor= (-dtp * amptuple[n] + dtm * amptuple[n + 1]) /\
	    		(dtm - dtp)
			self.setStrength(factor)					

class SineMagnetWaveform(MagnetWaveform):
	"""
	The magnet strength variation subject to a sine function
	f(t) = a+b*sin(w*t+p)  @Xiaohan Lu/CSNS, luxh@ihep.ac.cn
	"""
	def __init__(self, name = "no name"):
		MagnetWaveform.__init__(self, name)
		self.__initial = []
		
	def initialize(self,a,b,w,p):
		self.__initial = (a,b,w,p)
		
	def calc(self, t):		
		(a,b,w,p) = self.__initial
		f = a+b*math.sin(w*2*math.pi*t+p*math.pi)
		self.setStrength(f)

class FourierMagnetWaveform(MagnetWaveform):
	"""
	The magnet strength variation subject to a Fourier series
	f(t) = a+b*sin(w*t+p)+b1*sin(2*w*t+p1)+b2*sin(3*w*t+p2)......
	@Xiaohan Lu/CSNS,luxh@ihep.ac.cn
	"""
	def __init__(self, a,b,w,p,name = "no name"):
		MagnetWaveform.__init__(self, name)
		self.__initial = (a,b,w,p)
		
	def calc(self, t):		
		(a,b,w,p) = self.__initial
		if (not isinstance(b,list)) or (not isinstance(p,list)):
			orbitFinalize("The parameters b and p should be a list in the FourierMagnetWaveform")
		if (len(b) != len(p)):
			orbitFinalize("The length of the list b should be equal to the length of p!")
		f = a
		order = len(b)
		for i in xrange(order):
			f=f+b[i]*math.sin((i+1)*w*2*math.pi*t+p[i]*math.pi)
		self.setStrength(f)
                
class ErrorWaveform(Waveform):
	"""
	The subclass of Waveform class. The abstract class of the error waveforms.
	"""
	def __init__(self, name = "no name"):
		Waveform.__init__(self, name)
		self.setType("error waveform")
		self.strength = 0.0
	
	def setStrength(self,strength):
		self.strength = strength

	def getStrength(self):
		return self.strength

class RandomErrorWaveform(ErrorWaveform):
	"""
	The subclass of Waveform class. The abstract class of the magnet waveforms.
	"""
	def __init__(self, a, b, name = "no name"):
		ErrorWaveform.__init__(self, name)
                self.__initial=(a,b)
	
	def calc(self,t):
                (a,b) = self.__initial
                self.strength = gauss(a,b)


